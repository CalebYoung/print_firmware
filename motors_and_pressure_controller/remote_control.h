void fire(); //declaring fire() to end circular dependancy
void initiatePressureSystem();
void fire_no_motors();
void fire_xaar1();
void fire_xaar2();
double opentime;

// remote control code 
#include <IRremote.h>

int RECV_PIN = 11;

IRrecv irrecv(RECV_PIN);

decode_results results;

void remote(int selected_entry){
    switch(selected_entry){
    
    // 0: just cure LED 
    case(0xFF6897): for (int i =0; i<10; i++){ UV_LED(30000,"L");} break;//longCuring(); break;

    // CH+: fire  
    case(0xFFE21D):
    if (firstFire){findHome(); firstFire = false;}
    firing = switch_any(firing);
    digitalWrite(pause_LED,firing); break;

    // CH-: fire_xaar1
    case(0xFFA25D): fire_xaar1(); break;

    // CH: fire_xaar2
    case(0xFF629D): fire_xaar2(); break;
    
    //<<<PRESSURE SYSTEM>>>

    // EQ: turn feedback pressure system on and off
    case (0xFF906F): initiatePressureSystem(); break;

    // 200+: increase pressure max
    case(0xFFB04F): valveControl("open"); while(digitalRead(LSR)==LOW){pressure_motor(100,240,"extend");} valveControl("close"); break;

    // 100+: decrease pressure min
    case(0xFF9867): valveControl("open"); while(digitalRead(LSE)==LOW){pressure_motor(100,240,"retract");} valveControl("close"); break;
    
    // >>|: increase pressure large
    case(0xFF02FD): opentime = millis(); valveControl("open"); pressure_motor(100,240,"extend"); break;
    
    // |<<: decrease pressure large
    case(0xFF22DD): opentime = millis(); valveControl("open"); pressure_motor(100,240,"retract"); break;
    
    // VOL +:increase pressure small 
    case(0xFFA857): opentime = millis(); valveControl("open"); pressure_motor(10,100,"extend"); break;

    //VOL -: decrease pressure small
    case(0xFFE01F): opentime = millis(); valveControl("open"); pressure_motor(10,100,"retract"); break;

    //>|| : select which tanks pressure is going to
    case(0xFFC23D): tankSelector(); break;
    
    //<<<GANTRY MOVEMENT>>>
    // 2: move printhead up  
    case(0xFF18E7): Z_motor(10,120,"up",MICROSTEP); break;

    // 4: move printhead left  
    case(0xFF10EF): X_motor(30,10,"left"); break;
    
    // 6: move printhead right  
    case(0xFF5AA5): X_motor(30,10,"right"); break;

    // 8: move printhead down  
    case(0xFF4AB5): Z_motor(10,120,"down",MICROSTEP); break;

    // 1: move printbed back
    case(0xFF30CF): Y_motor(64,10,"backward"); break;

    // 7: move printbed forward
    case(0xFF42BD): Y_motor(64,10,"forward"); break;
    
    // 5: find home and reset
    case (0xFF38C7): findHome(); YrowPos= 0; offsetNum = 0; break;

    // 9: move printhead down fast 
    case(0xFF52AD): Z_motor(150,240,"down",INTERLEAVE); break;

    // 3: move printhead up fast  
    case(0xFF7A85): Z_motor(150,240,"up",INTERLEAVE); break;
    
}

irrecv.resume();
}