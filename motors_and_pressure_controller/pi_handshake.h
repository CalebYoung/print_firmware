//Serial.setTimeout(1000000); //20 min

const int cameraOffsetX = 300; //amount camera is offset form printhead in the X axis was 700
const int cameraOffsetY = 1200; //amount camera is offset form printhead in the Y axis //was 400
const int scanSpeed = 200;

String command[6];
char buf[16];
int bufLen;
int Y_row = 0;
void visionScan(){
  //move camera into position
  findHome();
  Y_motor(cameraOffsetY,5,"forward");
  X_motor(cameraOffsetX,5,"right");

  while(true){

      //handshake code
      Serial.print("P"); //Arduino Ready
      while(Serial.available()<1){} //Pi Ready (real time hold, any byte from pi should trigger)
      while(Serial.available()>0){Serial.read();}
      
      
      Serial.print("Mega starting row\n");
      X_motor(print_length-70,scanSpeed,"right");
      X_motor(print_length-70,4,"left");
      if(Y_row>Y_rows-3){Y_row = 0;break;} //break here to avoid moving Y one step in wrong direction before resetting
      Y_motor(256,4,"backward");
      Y_row++;
  }
  Y_motor(256*(Y_rows-1),4,"forward");
  delay(500);
  // return printheads to printing position
  findHome();
  }

bool piHandshake(){
    Serial.println("request from Mega to fire");
    //waiting for fire or vision command
    memset(buf, 0, sizeof(buf)); //clears buf
    buf[0]='a'; //clears buf
    bufLen = Serial.readBytesUntil(0x55, buf, 16);

    // normal fire
    if (buf[0] == 'f'){
      return true;}
    
    //scanning time! 
    if ((buf[0]) == 'v'){
      visionScan();
      return false;
    }
    
    //if printing finished
    if (buf[0] == 'd'){
      firing = false;} //findHome();}
      return false;
    return false; //incase mega doesn't hear from pi
}


