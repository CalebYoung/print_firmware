const int xaar_2_delay = 142;     //number of motor steps that xaar 2 is behind xaar 1 (was 130)
float XoffsetScale = .647;     //pixels per motor step


//when coding Z axis     myMotor2->release(); to release pressure system then myMotor1->release(); to release z axis motors
#include <Wire.h>
#include <Adafruit_MotorShield.h>

// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 
// Or, create it with a different I2C address (say for stacking)
// Adafruit_MotorShield AFMS = Adafruit_MotorShield(0x61); 
// Connect a stepper motor with 200 steps per revolution (1.8 degree)
// to motor port #1 (M1 and M2)
Adafruit_StepperMotor *myMotor1 = AFMS.getStepper(200, 1);
// to motor port #2 (M3 and M4)
Adafruit_StepperMotor *myMotor2 = AFMS.getStepper(200, 2);

//functions for Z motors
void Z_motor(int moves,int Z_speed,char dir[],char mode){
  myMotor1->setSpeed(Z_speed);
  if (dir == "up"){myMotor1->step(moves, FORWARD, mode);}
  else {myMotor1->step(moves, BACKWARD, mode);}
  myMotor1->release();
 }

//functions for X motor
void X_motor (int moves, int X_speed, char dir[]){
  digitalWrite(X_dirPin, (dir == "right")); //SETS DIR PIN yields HIGH for left, LOW for right

  for (int i=moves;i>0;i--){
    if (X_pos > LSXR && dir != "left"){break;} //check limit switch(digital)
    else if (digitalRead(LSXL) == LOW && dir == "left"){break;} //check limit switch
    xPos += 1-2*(dir == "left"); //adds one for right movement, subracts one for left
    
    //"micromovment"
    for (int i = 0; i < 100; i++){//multiplies movement by 10 so for loop doesn't max out 
    digitalWrite(X_stepPin, HIGH);
    digitalWrite(X_stepPin, LOW);
    delayMicroseconds(X_speed);} 
  
}
}

//functions for Y motor
void Y_motor (int moves, int Y_speed, char dir[]){
  digitalWrite(Y_dirPin, (dir == "forward")); //SETS DIR PIN yields HIGH for left, LOW for right
  for (int i=moves;i>0;i--){
    if (digitalRead(LSYB)==LOW && dir != "forward"){break;} //check limit switch
    yPos += 1-2*(dir == "forward"); //adds one for backward movement, subracts one for forward
    //"micromovment"
    for (int i = 43;i>0;i--){//multiplies movement by 10 so for loop doesn't max out //////////85 without microstepping
    digitalWrite(Y_stepPin, HIGH);
    digitalWrite(Y_stepPin, LOW);
    delayMicroseconds(Y_speed);} 
  
}
}


//functions for pressure motor
void pressure_motor(int moves, int P_speed, char dir[]){
  myMotor2->setSpeed(P_speed);
  if (dir == "retract"){
    if (digitalRead(LSE)==LOW){myMotor2->step(moves, BACKWARD, DOUBLE);} // retract 
    else{needToResetPressure = true; myMotor2->step(moves, BACKWARD, DOUBLE); //Piston retract limit reached, continue to move back up to ten times to equilize
    emergencyPressureShutOff++; if(emergencyPressureShutOff > 25){delay(9999999999);}
   }
  }

  else if (dir == "extend" && digitalRead(LSR)==LOW){
    myMotor2->step(moves, FORWARD, DOUBLE);}
    myMotor2->release();
    }



