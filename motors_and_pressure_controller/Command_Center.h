//Pressure system
float setPressure1 = 0;         // for tank 1 in PSI
float setPressure2 = 0;          // for tank 2 in PSI

//curing------------------------------
bool curing = false;               //true = curing on
//const int curingFreq = 1;          //cure every nth layer
const int curingSpeed = 100;        //speed of motors during curing step (lower number makes motor move faster, which results in less curing)
//int curingCount = 0;               //set negative for extra layers before first cure

//X axis------------------------------
const int print_length = 870;      //number of motor steps (set by switches on side of stepper driver) (was 1030)
const int X_speed = 10;            //microsecond delay between steps (4 = max speed, 100 is very slow)

//Z axis------------------------------
const float layerHeight = 30;     //in microns
