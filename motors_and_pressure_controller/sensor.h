int sensorCount1 = 0;
int sensorCount2 = 0;
float sensorDataPoint1;
float sensorValue1 = 0;
float sensorValue2 = 0;

void sensor1(){
  sensorValue1 += analogRead(sensorPin);
  if(sensorCount1 > 8){//reads sensorValue 10 times
      float pressure_map = map(sensorValue1/10,191,460,-1000,1000);
      currentPressure1 = pressure_map/1000 +.38;
      sensorCount1 = 0;
      sensorValue1=0;
      }
  sensorCount1 ++;
}

void sensor2(){
  sensorValue2 += analogRead(sensorPin2);
  if(sensorCount2 > 8){//reads sensorValue 10 times
      float pressure_map = map(sensorValue2/10,191,460,-1000,1000);
      currentPressure2 = pressure_map/1000 +.38;
      sensorCount2 = 0;
      sensorValue2 = 0;}
  sensorCount2 ++;
}
