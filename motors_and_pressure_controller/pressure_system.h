bool pressureSystem2 = true;       // second pressure system active?
bool pressureLockState = false;    // true: pressure locks at current valve. false: pressure goes to "setPressure" (next line)

void initiatePressureSystem(){
  pressure_feedback_state = switch_any(pressure_feedback_state);
     digitalWrite(press_LED,pressure_feedback_state);  //triggers indicator light
     if(!pressure_feedback_state){pressureLock1=0; pressureLock2=0;return;}
     
     if (pressureLockState== true){
        pressureLock1 = currentPressure1;
        pressureLock2 = currentPressure2;
     }
     else{
         pressureLock1 = setPressure1;
         pressureLock2 = setPressure2;
        }
}

int lowCount1 = 0;
int lowCount2 = 0;
int highCount1 = 0;
int highCount2 = 0; 
int pressureCount = 0;   

void resetPressure(){
  digitalWrite(releaseValve,HIGH); //open valve return piston shut other two valves
  while(digitalRead(LSR)==LOW){myMotor2->step(240,FORWARD,DOUBLE);}
  digitalWrite(releaseValve,LOW);
  needToResetPressure=false;
  }

void pressureEquilizer(){
pressureCount ++;
if(pressureCount > 8){
emergencyPressureShutOff = 0; //reset emergency shutoff feature
if(needToResetPressure){resetPressure();}
//tank 1
if(abs(currentPressure1-pressureLock1)>.05){ //triggered to adjust pressure
    
    if (currentPressure1<pressureLock1){ //If pressure is too low
        delay(10); //let electrical signals smooth out? 
        lowCount1++;
        if(lowCount1>20){//pressure off 3 times in a row, adjust
            //////////Serial.println("TANK 1 LOW PRESSURE");
            digitalWrite(pressureValve1,HIGH);
            while (currentPressure1<pressureLock1+.02){
                pressure_motor((abs(currentPressure1-(pressureLock1+.07))*120+1),200,"extend");
                delay(50);
                for(int i = 0; i < 10; i++){sensor1();}
                Serial.println(currentPressure1);
            }
            digitalWrite(pressureValve1,LOW);
        }
    }
    
    else{ //pressure is too high
        delay(10); //let electrical signals smooth out? 
        highCount1++;
        if(highCount1>20){//pressure off 3 times in a row, adjust
          /////////////Serial.println("TANK 1 HIGH PRESSURE ");
          digitalWrite(pressureValve1,HIGH);
          
            while (currentPressure1>pressureLock1-.02){
                pressure_motor((abs(currentPressure1-(pressureLock1-.07))*120+1),200,"retract");
                delay(50);
                for(int i = 0; i < 10; i++){sensor1();}
                Serial.println(currentPressure1);
            }
            digitalWrite(pressureValve1,LOW);
        }
    }

}
else{lowCount1 = 0; highCount1 = 0;}

if(pressureSystem2){
//tank 2
if(abs(currentPressure2-pressureLock2)>.05){ //triggered to adjust pressure
  
    if (currentPressure2<pressureLock2){ //If pressure is too low
        delay(10); //let electrical signals smooth out? 
        lowCount2++;
        if(lowCount2>20){//pressure off 5 times in a row, adjust
            //////////Serial.println("TANK 2 LOW PRESSURE ");
            digitalWrite(pressureValve2,HIGH);
            while (currentPressure2<pressureLock2+.02){
                pressure_motor((abs(currentPressure2-(pressureLock2+.07))*120+1),200,"extend");
                delay(50);
                for(int i = 0; i < 10; i++){sensor2();}
                Serial.println(currentPressure2);
            }
            digitalWrite(pressureValve2,LOW);
        }
    }
    
    else{ //pressure is too high
        delay(10); //let electrical signals smooth out? 
        highCount2++;
        if(highCount2>20){//pressure off 5 times in a row, adjust
            /////////Serial.println("TANK 2 HIGH PRESSURE ");
            digitalWrite(pressureValve2,HIGH);
            while (currentPressure2>pressureLock2-.02){
                pressure_motor((abs(currentPressure2-(pressureLock2-.07))*120+1),200,"retract");
                delay(50);
                for(int i = 0; i < 10; i++){sensor2();}
                Serial.println(currentPressure2);
            }
            digitalWrite(pressureValve2,LOW);
        }
    }
}
else{lowCount2 = 0; highCount2 = 0;}
pressureCount = 0;}

}
}

void tankSelector(){//chooses which tanks are active 
  if      (tank1State && !tank2State){tank1State=0; tank2State=1; digitalWrite(tank1LED,LOW);  digitalWrite(tank2LED,HIGH);}
  else if (!tank1State && tank2State){tank1State=1; tank2State=1; digitalWrite(tank1LED,HIGH); digitalWrite(tank2LED,HIGH);}
  else if (tank1State && tank2State) {tank1State=0; tank2State=0; digitalWrite(tank1LED,LOW);  digitalWrite(tank2LED,LOW);}
  else                               {tank1State=1; tank2State=0; digitalWrite(tank1LED,HIGH); digitalWrite(tank2LED,LOW);}
  }

void valveControl(char dir[]){//closes and opens active tanks
  if (dir == "open"){
    {digitalWrite(pressureValve1,LOW);}
    if (tank1State){digitalWrite(pressureValve1,HIGH);}
    if (tank2State){digitalWrite(pressureValve2,HIGH);}
  }
  else{
      if (tank1State){digitalWrite(pressureValve1,LOW);}
      if (tank2State){digitalWrite(pressureValve2,LOW);}
  }
}



