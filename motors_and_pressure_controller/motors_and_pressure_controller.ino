
#include "command_center.h"
int dumberror = 0;
int up_count_value =0; //used to control how often the Z axis moves
int incomingByte = 0;
//communication with Xaar 128 drivers
const int xaar1_out = 6;
const int xaar2_out = 5;


bool fire_permission = 0;

//IO pins
const int uv_led_switch_L = 31; //turns UV LED on and off
const int uv_led_switch_R = 30;
const int ir_led_switch;//pin not added
//state buttons
bool firstFire = true;
bool firing = false;//start/pause button
bool pressure_feedback_state = false; //turns pressure feedback system on and off

//indicator lights
const int press_LED = 12;
const int pause_LED = 13; //TODO Find a pin for firing LED



//pressure system

const int tank1LED = 3;
const int tank2LED = 4;
float sensorPin = A2;/////sensor analog input
float sensorPin2 = A1;
float sensorValue;
float pressure;
float currentPressure1;
float currentPressure2;
float pressureLock1;
float pressureLock2;
bool tank1State = 1; //used for remoteSelector()
bool tank2State = 0; //used for remoteSelector()
double valveTimeout;
const int releaseValve = 46;
const int pressureValve1 = 48;
const int pressureValve2 = 49;
bool needToResetPressure = false; //this code is triggered on so pressure reset happens after pressure is equalized
int emergencyPressureShutOff = 0; //used to allow pressure system to move a few times past its limit so pressure can be equilized before piston is reset
float set_pressure=0;/////What the pressure is set to
int serial_monitor_count=0;//so the serial monitor doesn't print every loop
int consecutive_cycles = 10; //how far pressure system moves 
//X axis motor
int Zcount = 0;

//X axis motor
int xPos; //used for return fire sequence
const int X_stepPin = 53;
const int X_dirPin = 52;
const int X_enblPin = 51;

//Y axis motor
int Y_rows; //initiated by pi
int yPos;   //current position of Y by motor steps
int YrowPos = 0; //current position of Y by row
const int Y_stepPin = 45;
const int Y_dirPin = 44;
const int Y_enblPin = 43;
bool microOffsetState = true;

//limit switch inputs

int X_pos = 0; //used to keep track of x position 
const int LSE = 7;//stops cylinder from extending
const int LSR = 8;//stops cylinder from retracting ///on outside of block wiring
const int LSXL = 9; //stops X_motor from moving left
const int LSYB = 26; //stops Y_motor from moving back
const int LSXR = 5000;// digital limit switch

//offsetting code
int offsetNum = 0;

const int offsetList[128] = {0, 36, 55, 126, 33, 65, 84, 60, 24, 62, 35, 104, 43, 70, 110, 73, 91, 3, 64, 114, 71, 103, 75, 113, 58, 80, 83, 95, 6, 13, 97, 117, 51,
                              86, 23, 59, 12, 37, 81, 31, 88, 101, 50, 67, 8, 17, 48, 123, 38, 120, 90, 74, 72, 79, 87, 66, 20, 121, 56, 26, 109, 78, 99, 4, 82, 29, 5,
                              16, 2, 39, 89, 49, 9, 108, 106, 54, 44, 19, 125, 11, 94, 122, 45, 76, 25, 30, 57, 22, 61, 53, 107, 105, 42, 41, 127, 124, 47, 96, 111, 92,
                              77, 118, 100, 32, 112, 15, 40, 63, 28, 34, 1, 46, 93, 115, 68, 14, 119, 10, 7, 27, 102, 116, 52, 18, 21, 98, 69, 85};
