void setup(){
  const int Y_dirPin = 0;
  const int Y_enblPin = 17;
  Wire.setClock(400000);//set I2C communication faster for motorshield 
  Serial.begin(9600); 
  Serial.setTimeout(1000);

  //remote control code
  irrecv.enableIRIn();
  while(!Serial){}
  Serial.println("Hello from Mega");

  ///////////////////////////////////////////////Stepper motor code/////////////////////////////////////////
  AFMS.begin();  // create with the default frequency 1.6KHz
  myMotor1->setSpeed(50);  // 20 rpm
  myMotor2->setSpeed(50);  // 10 rpm 

  /////////////////////////////////////////////////Initilize Pins///////////////////////////////////////////
  pinMode(X_stepPin,OUTPUT);
  pinMode(X_dirPin,OUTPUT);
  pinMode(X_enblPin,OUTPUT); 
  pinMode(Y_stepPin,OUTPUT);
  pinMode(Y_dirPin,OUTPUT);
  pinMode(Y_enblPin,OUTPUT);
  pinMode(11,INPUT);
  pinMode(LSE,INPUT);
  pinMode(LSR,INPUT);
  pinMode(LSXL,INPUT);
  pinMode(LSYB, INPUT);
  pinMode(pressure_feedback_state,OUTPUT);
  pinMode(pressureValve1,OUTPUT);
  pinMode(pressureValve2,OUTPUT);
  pinMode(releaseValve,OUTPUT);
  pinMode(uv_led_switch_L,OUTPUT);
  pinMode(uv_led_switch_R,OUTPUT);
  pinMode(ir_led_switch,OUTPUT);
  pinMode(xaar1_out,OUTPUT);
  pinMode(xaar2_out,OUTPUT);
  pinMode(tank1LED,OUTPUT);
  pinMode(tank2LED,OUTPUT);
  pinMode(pause_LED,OUTPUT); //led indicator for firing button
  pinMode(press_LED,OUTPUT); //led indicator for pressure system
  
  // initial states
  digitalWrite(press_LED,LOW);
  digitalWrite(tank1LED,HIGH);
  digitalWrite(Y_enblPin,HIGH);
  digitalWrite(pressureValve1,LOW);
  digitalWrite(pressureValve2,LOW);
  digitalWrite(releaseValve,LOW);
  digitalWrite(uv_led_switch_L,HIGH);
  digitalWrite(uv_led_switch_R,HIGH);

  ////////////////////////////////////////////////initiate info from pi////////////////////////////////////
  remote(0xFF906F);   //turn on pressure
  while(Serial.available()<1){
    if (pressure_feedback_state){pressureEquilizer();
    }
    
    if(irrecv.decode(&results)){ //if remote detects an entry
      remote(results.value);}
    }

  // read the incoming byte:
  incomingByte = Serial.read();
  Y_rows = incomingByte;

  Serial.print("Y_rows: ");
  Serial.println(Y_rows);
      
  }
