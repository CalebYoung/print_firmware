void fireRow(){
    X_motor(2,X_speed,"right"); //initial acceleration 
    digitalWrite(xaar1_out,HIGH);

    X_motor(1,X_speed,"right"); //allow time for signal to process
    digitalWrite(xaar1_out,LOW);
    
    X_motor(xaar_2_delay,X_speed,"right"); //fire xaar 2
    digitalWrite(xaar2_out,HIGH);
    
    X_motor(1,X_speed,"right"); //allow time for signal to process
    digitalWrite(xaar2_out,LOW);

    X_motor(print_length,X_speed, "right");
    if(curing){digitalWrite(uv_led_switch_L,LOW);}
   
    //X_motor(print_length-250,X_speed,"right");
    if(curing){digitalWrite(uv_led_switch_L,HIGH);}
    
    //X_motor(420,3,"left");
    if(curing){digitalWrite(uv_led_switch_R,LOW);}
    
    //X_motor(270,3,"left");
    if(curing){digitalWrite(uv_led_switch_R,HIGH);}
    
    //return printhed

    while(xPos>0){X_motor(1,3,"left");}
    
}

void LEDcuring(){
      //forward movement
      digitalWrite(uv_led_switch_R,LOW);
      X_motor(350,curingSpeed,"right");
      digitalWrite(uv_led_switch_L,LOW);
      X_motor(150,curingSpeed,"right");
      digitalWrite(uv_led_switch_L,HIGH);
      digitalWrite(uv_led_switch_R,HIGH);
      //sideways (Yaxis) movement
      while(yPos>0){Y_motor(1,3,"forward");}
      //backward movement
      digitalWrite(uv_led_switch_L,LOW);
      digitalWrite(uv_led_switch_R,LOW);
      X_motor(150,curingSpeed,"left");
      digitalWrite(uv_led_switch_L,HIGH);
      X_motor(350,curingSpeed,"left");
      digitalWrite(uv_led_switch_R,HIGH);
}

// moves the Z up a determined amount based on layerHeight in Command_Center.h
void moveZup(){
    Zcount++;
    //Z and pressure adjust
    if (Zcount > 22.2/layerHeight){
        Z_motor(1,50,"up",MICROSTEP);
        Zcount=0;}
}
  
void microOffset(){
  if (microOffsetState){Y_motor(1,5,"forward"); microOffsetState = 0;}
  else {microOffsetState = 1;}
}

void offset(){
    if(offsetNum == 127){offsetNum = 0;}
    else{offsetNum++;}
    Y_motor(offsetList[offsetNum]*2,3,"forward"); //the "*2" is for micro offsetting
}

void fire(){
    //Xoffset
    X_motor(offsetList[offsetNum]*XoffsetScale,5,"left"); 

    fireRow(); //code for moving printhead pack and forth
    
    //Y axis code
        if (YrowPos > (Y_rows-2)){//needs to return
          //LEDcuring();
          while(yPos<0){ //reset to first row ***********switched > to < and backward with forward
          Y_motor(1,3,"backward");}
          microOffset();
          YrowPos=0;
          offset();
          moveZup();
        }
        else{Y_motor(256,3,"forward");//needs to move one row
        YrowPos++;
        }

    if (pressure_feedback_state){pressureEquilizer();}//adjusts pressure

    if(irrecv.decode(&results)){ //if remote detects an entry
      remote(results.value);}
    irrecv.resume();//clear remote

}

void fire_xaar1(){
     digitalWrite(xaar1_out,HIGH);
     delay(50);
     digitalWrite(xaar1_out,LOW);
     delay(1500);
}

void fire_xaar2(){
     digitalWrite(xaar2_out,HIGH);
     delay(50);
     digitalWrite(xaar2_out,LOW);
     delay(1500);
}


