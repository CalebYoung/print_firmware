#ifndef XAAR128_H_
#define XAAR128_H_


//For arduino Zero
// ZERO SPI: SCK: 9, MISO: 10, MOSI: 8
// Supply / Supply Ctrl pins
#define nSS1          11 // Chip select for XARR 128 1st IC
#define nSS2           5 // Chip select for XARR 128 2nd IC
#define relayVHCH     3 // Relay for high current
#define relayVHCL     4 // Relay for low current
#define nCLK          7 // 1MHz clock
//#define SD_1          4 OBSOLETE
//#define xVDD           4

//end Arduino Uno specific code

// Output pins
//int PHO = 5;
#define nRESET           14
#define nFIRE            13

// Input pins
#define READY            12 // This will be an interrupt pin.
#define nROWS            2
#define nCOLS            8

class Xaar128 {

public:
  int readyState = 0;

  Xaar128();
  void init();
  void powerUp();
  void powerDown();
  void loadBuffer64(byte *val);
  void loadData(byte buf[nROWS][nCOLS]);
  void go(int pending);
  bool fire();
};

#endif