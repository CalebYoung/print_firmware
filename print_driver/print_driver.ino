//runs on ARDUINO ZERO

#include "Arduino.h"
byte currentLayer[967][16];
#include <HID.h>
#define SERIAL_BUFFER_SIZE 512
#include <xaar128.h>

//initiate variables
const int layerPercent = 100; //firing percentage of printheads
const int start_layer = 0; //Layers to skip before printing
const int row_delay = 100; //in microseconds
const int repeat_fires = 15; //fires the same row multiple times
const int num_layers = 10000; //set to 10,000 to print all layers
const int num_rows = 2; // set to 4 for now, needs to be dynamic based on num of images in each subfolder
char sizeOfArray[6];
int row = 0;
int divider = 50; //for layerEquilizer function
boolean newData = false;
const int motors_in = 18; //A3
int numOfRows;
int bufLen;
char buf[16];
// code to fire all nozzels (used for debugging)
#byte fullBuf[2][8] =  {{0b11111111,0b11111111,0b11111111,0b11111111,0b11111111,0b11111111,0b11111111,0b11111111},{0b11111111,0b11111111,0b11111111,0b11111111,0b11111111,0b11111111,0b11111111,0b11111111}};

//initiate library to bitshift bytes into printdrivers
Xaar128 xaar128;

void setup () {

  Serial.begin(9600);
  Serial.setTimeout(1000);
  pinMode(motors_in,INPUT);
  setClock1MHz();

  //////////////////////// 1) connect to Pi //////////////////////////////////
  while (!Serial){}
  Serial.println("helloooo from Xaar");

  //////////////////////// 2) Initiate Array Size ////////////////////////////
  bufLen = Serial.readBytesUntil(0x55, buf, 16);
  for(int i = 0; i < bufLen; i++){
      sizeOfArray[i]=buf[i];
  }
  numOfRows = atoi(sizeOfArray);
  Serial.print("Arduino Array Size: ");
  Serial.println(String(numOfRows));

  xaar128.init();

  float fails = 0.0;
  float steps = 0.0;
  delay(500);

  //////////////////////// 3) load array from Pi  ////////////////////////////
  while (buf[0] != 0x54){ //master loop for all files on Pi
  row = 0;
      while(true){

      while(Serial.available()==0){}
      Serial.readBytes(buf,16); //reads 16 bytes into bufm
      if (buf[0]==0x55){break;} //end of array found
      //place data into array
      for (int i = 0; i <16; ++i){
        currentLayer[row][i]=buf[i]; // buf[1] changed to 254
      }
      row++;
      }

      //handshake code
      
      //analogWrite(motors_out, 255);//send motors permission to fire
      while((analogRead(motors_in))<80){};//wait for fire signal
      //analogWrite(motors_out, 0);//avoids double firing

      xaar128.powerUp();

      //code to print one layer
      for (int row_ = 0; row_ < numOfRows; ++row_){//loop for all rows in layer
        //code to print one row
        //Serial.println("1");
        //break 16 buf array into [2][8] buf
      for (int i = 0; i < 8; i++){tempBuf[0][i] = currentLayer[row_][i];}
      for (int j = 0; j < 8; j++){tempBuf[1][j] = currentLayer[row_][j+8];}
        //code to reduce percent of nozzle fires to even print layers out between different inks
        /*
        for(int i = 0; i < repeat_fires; ++i){

            divider -= layerPercent;
            if (divider < 0){
                divider += 100;
                xaar128.loadData(buf);
            }
            else{xaar128.loadData(blankBuf);}
          */
          // send data to printhead
          xaar128.loadData(tempBuf);
          delayMicroseconds(row_delay);//delay between nozzels firing
          xaar128.fire();
          delayMicroseconds(row_delay);//delay between nozzels firing
          //record pass/fail ratio 
          if (!xaar128.fire()) fails++;
            steps++;
}
  xaar128.powerDown();
  printSummary(fails, steps);
  }//closes out master for loop
  Serial.println("Zero finished print");

  showNewData();
  printSummary(fails, steps);
  
  }//end setup

  void showNewData() {
      Serial.println("\nImage data received:\n");
      Serial.println((char*)buf);
  }

  void printSummary(float fails, float steps) {
      float f = fails / steps;
      Serial.print("Steps: ");
      Serial.print(steps);
      Serial.print("    Fails: ");
      Serial.print(fails);
      Serial.print("    Failure Rate: ");
      Serial.println(f, 4);
  }

  // initiate Arduino Zeros internal clock to 1MHz 
  void setClock1MHz(){
    {
    //https://forum.arduino.cc/index.php?topic=340198.0
    // Output 1MHz signal on digital pin 7

    // Set the clock generator 5's divisor to 2 (8Mhz / 8 = 1MHz)
    REG_GCLK_GENDIV = GCLK_GENDIV_DIV(8) |          // Set the clock divisor to 2
                      GCLK_GENDIV_ID(5);            // Set the clock generator ID to 5
    while (GCLK->STATUS.bit.SYNCBUSY);              // Wait for the bus to synchronise
    
    // Connect he 48MHz clock source to generic clock generator 5 and enable its outputs
    REG_GCLK_GENCTRL = GCLK_GENCTRL_OE |            // Enable the generic clock 5's outputs
                      GCLK_GENCTRL_IDC |           // Ensure the duty cycle is about 50-50 high-low
                      GCLK_GENCTRL_GENEN |         // Enable the clock generator 5
                      GCLK_GENCTRL_SRC_OSC8M |     // Set source to be 48MHz clock 
                      GCLK_GENCTRL_ID(5);          // Set the clock generator ID to 5
    while (GCLK->STATUS.bit.SYNCBUSY);              // Wait for the bus to synchronise
    
    // Output the clock on digital pin 7 (chosen as it happens to be one of generic clock 5's outputs)
    // Enable the port multiplexer (mux) on digital pin 7
    PORT->Group[g_APinDescription[7].ulPort].PINCFG[g_APinDescription[7].ulPin].bit.PMUXEN = 1; 
    // Switch port mux to peripheral function H - generic clock I/O
    // Set the port mux mask for odd processor pin numbers, D7 = PA21 = 21 is odd number, PMUXO = PMUX Odd
    PORT->Group[g_APinDescription[7].ulPort].PMUX[g_APinDescription[7].ulPin >> 1].reg |= PORT_PMUX_PMUXO_H;
  }
}

void loop () {}
