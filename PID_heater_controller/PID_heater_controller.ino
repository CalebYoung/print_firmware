// which analog pin to connect
const int THERMISTORPIN1 = A1;
const int THERMISTORPIN2 = A2;
const int THERMISTORPIN3 = A3;
const int THERMISTORPIN4 = A4;
// resistance at 25 degrees C
#define THERMISTORNOMINAL 10000      
// temp. for nominal resistance (almost always 25 C)
#define TEMPERATURENOMINAL 25   
// how many samples to take and average, more takes longer but is more 'smooth'
#define NUMSAMPLES 5
// The beta coefficient of the thermistor (usually 3000-4000)
#define BCOEFFICIENT 3950
// the value of the 'other' resistor
#define SERIESRESISTOR 10000    
 
uint16_t samples[NUMSAMPLES];
const int button = 9;
const int green_led = 8;
const int yellow_led = 12;
const int power_switch = 11 ;
bool button_state;
int print_output;

/////////////////////////////////////////////////PID_RelayOutput///////////////////////////////////////////////////
#include <PID_v1.h>

#define PIN_INPUT 0
#define RELAY_PIN_1 2
#define RELAY_PIN_2 3
#define RELAY_PIN_3 4
#define RELAY_PIN_4 5

//Define Variables we'll be connecting to
double Setpoint1, Setpoint2, Setpoint3, Setpoint4, Input, Output;

//Specify the links and initial tuning parameters
double Kp=3, Ki=1, Kd=15;
PID myPID1(&Input, &Output, &Setpoint1, Kp, Ki, Kd, DIRECT);
PID myPID2(&Input, &Output, &Setpoint2, Kp, Ki, Kd, DIRECT);
PID myPID3(&Input, &Output, &Setpoint3, Kp, Ki, Kd, DIRECT);
PID myPID4(&Input, &Output, &Setpoint4, Kp, Ki, Kd, DIRECT);

int Max_temp = 50; //triggers cuttoff if exceeded
int WindowSize = 100;
float currentTemp;
bool LED_state;
/////////////////////////////////////////////////PID_RelayOutput///////////////////////////////////////////////////

void setup(void) {
  Serial.begin(9600);
  analogReference(EXTERNAL);
  pinMode(RELAY_PIN_1,OUTPUT);
  pinMode(RELAY_PIN_2,OUTPUT);
  pinMode(RELAY_PIN_3,OUTPUT);
  pinMode(RELAY_PIN_4,OUTPUT);
  pinMode(THERMISTORPIN1,INPUT);
  pinMode(THERMISTORPIN2,INPUT);
  pinMode(THERMISTORPIN3,INPUT);
  pinMode(THERMISTORPIN4,INPUT);
  pinMode(yellow_led,OUTPUT); //tells you if actual temp is within range of set temp
  pinMode(green_led,OUTPUT); //tells you if heating system is on
  pinMode(button,INPUT_PULLUP); //turns power on and off
  pinMode(power_switch,OUTPUT);
  
  Setpoint1 = 40; // front of printhead 1
  Setpoint2 = 40; // back of printhead 1
  Setpoint3 = 40; // front of printhead 2
  Setpoint4 = 40; // back of printhead 2

  //tell the PID to range between 0 and the full window size
  myPID1.SetOutputLimits(0, WindowSize); myPID2.SetOutputLimits(0, WindowSize); myPID3.SetOutputLimits(0, WindowSize); myPID4.SetOutputLimits(0, WindowSize);
  //turn the PID on
  myPID1.SetMode(AUTOMATIC); myPID2.SetMode(AUTOMATIC); myPID3.SetMode(AUTOMATIC); myPID4.SetMode(AUTOMATIC);
}

void loop(void) {
  for (int i = 1; i <5; i++){
      Input = thermistor(i);
      print_output = PID_output(i);
      Serial.print("Temp ");Serial.print(i); Serial.print(" "); Serial.print(thermistor(i)); Serial.print(" ("); Serial.print(print_output); Serial.print("%)   ");
  }
Serial.println();
  
  //LED warning light code
  // if either Xaar is more than 3 degrees off its setpoint
  if ((abs(static_cast<int>(thermistor(1)) - Setpoint1) > 3)&& (abs(static_cast<int>(thermistor(2)) - Setpoint2) > 3) &&LED_state == 0){digitalWrite(yellow_led,HIGH); LED_state = 1;}
  // if it falls back in range
  else if((abs(static_cast<int>(thermistor(1)) - Setpoint1) < 3) && (abs(static_cast<int>(thermistor(2)) - Setpoint2) < 3) && LED_state == 1){digitalWrite(yellow_led,LOW); LED_state = 0;}

  //power switch logic (button on breadboard)
  if(digitalRead(button)==LOW){
    while(true){
      if(digitalRead(button)==HIGH){
        button_state = switch_any(button_state);
        Serial.print(button_state);
        break; //exits power switch logic
      }
    }
  }

}
