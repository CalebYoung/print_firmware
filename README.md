# Avana's 3D Printer Firmware Overview
![](readme_images/entire_printer.jpg)

## Overall design and print drivers
**Version 1** printdrivers ran off of 8 bit microcontrollers. These controllers read data off of an SD card and fed it to the printheads one firing sequence at a time.

**Version 2** The vision system's correction technique meant dynamicly loading print data was necessary. The 8 bit micro controllers were upgraded to 32 bit microcontrollers which loaded an entire loayer from the raspberry pi.

| | | 
|:-------------------------:|:-------------------------:|
|<img height="500" alt="glasses" src="readme_images/Avana_Prototype_Layout.png">  wiring diagram (version 1) | <img height="500" alt="lens" src="readme_images/print_drivers.png"> print controllers (version 2)|

## PID heater controller:
Precise heat control of the printheads is required. This code runs on an 8 bit micro controller to simultaneously run 4 different PID control loops (4 sensors, 4 heating elements). accuracy is within a fraction of a degree(celcius).

**Format of print statments (Celcius)**
```
Temp 1: recorded value (% power)   Temp 2: recorded value (% power)   Temp 3: recorded value (% power)   Temp 4: recorded value (% power)   
```

![](readme_images/heater_cap.mp4)

## Pressure System
Precise pressure control is required to insure the ink reaches the printhead but does not drip out. This code runs on an arduino to control two different tanks to within .3 PSI. Due to noise in the data several out of spec measurements in a row must be recorded to trigger a pressure correction. This is important as the pressure correction temporarily halts the printer.

**Format of print statements (PSI)**
```
tank 1: recorded value (target)     tank 2: recorded value (target)
```

![](readme_images/pressure_system_cap.mp4)